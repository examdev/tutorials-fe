# base image 
FROM node:18 as build 
# container workspace 
WORKDIR /app 
# Bring in package dependencies
COPY package*.json ./ 
# Install react
RUN npm install 
# copy all project files into the container's workspace 
COPY . . 
# react production environement
RUN npm run build 
# use nginx with the tag mainline-alpine3.18 as your base image
FROM nginx:alpine 
# copy the result of the build under /usr/share/nginx/html/
COPY --from=build /app/build /usr/share/nginx/html 
# Make port 80
EXPOSE 80 
# Automatically start Nginx in the foreground when the container runs 
CMD ["nginx", "-g", "daemon off;"]